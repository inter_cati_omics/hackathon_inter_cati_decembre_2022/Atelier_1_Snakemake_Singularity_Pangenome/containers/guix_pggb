# guix_pggb

## guix, a short tutorial 

https://guix.gnu.org/

https://hpc.guix.info/

#### Paper on reproducible bioinformatics pipelines with Guix
https://guix.gnu.org/en/blog/2018/paper-on-reproducible-bioinformatics-pipelines-with-guix/


## Getting started

#### install guix on a Linux machine
```
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
chmod +x guix-install.sh
sudo ./guix-install.sh
```

#### add profile to your .bashrc
```
GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"
```
and
```
source ~/.bashrc
```

#### Update tree
```
guix pull && guix package -u
```

#### search 4 poackages & list
https://packages.guix.gnu.org/
```
guix search wget
guix package --list-installed
```

#### Basic packages locale
```
guix install glibc-locales
$ export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
```


#### install pggb
https://github.com/pangenome/pggb

```
git clone https://github.com/ekg/guix-genomics
cd guix-genomics
GUIX_PACKAGE_PATH=. guix package -i pggb
```


#### convert in singularity image
```
cd guix-genomics
GUIX_PACKAGE_PATH=. guix pack -f squashfs bash glibc-locales guile util-linux wget pggb
```

#### exe pggb from singularity
```
singularity exec /gnu/store/ij71ggmkmb0bdlbbfxnqn2nann32a49c-bash-glibc-locales-guile-util-linux-squashfs-pack.gz.squashfs pggb
```

